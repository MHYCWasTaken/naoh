#include <iostream>
using namespace std;

typedef unsigned int uint;
typedef unsigned short ushort;

class Way {
public:
    uint map;
    ushort length;
    ushort getClueType();
    ushort getClue();
    void setClue(ushort type, ushort c);
    ushort getBlock(ushort i);
    void setBlock(ushort i, ushort b);
private:
    ushort cnt;
    ushort clue;
    ushort maskA = (1 << 15) + (1 << 14); // 1100000000000000
    ushort maskB = ~maskA;                // 0011111111111111
};

void Way::setClue(ushort type, ushort c) {
    clue = type << 14 + c;
    return;
}

ushort Way::getClueType() {
    return (clue & maskA) >> 14;
}

ushort Way::getClue() {
    return clue & maskB;
}

ushort Way::getBlock(ushort i) {
    uint mask = (1 << ((i << 1) + 1)) + (1 << (i << 1));
    return (map & mask) >> (i << 1);
}

void Way::setBlock(ushort i, ushort b) {
    uint mask = ~((1 << ((i << 1) + 1)) + (1 << (i << 1)));
    map = (map & mask) + (b << (i << 1));
    return;
}


int main() {

}